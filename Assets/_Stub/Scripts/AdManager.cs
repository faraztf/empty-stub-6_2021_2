﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AOT;
using UnityEngine.SceneManagement;
using EasyMobile;
using System;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class AdManager : MonoBehaviour 
{
	public static AdManager instance;

	void Awake()
	{
		makeInstance();

		if (!EasyMobile.RuntimeManager.IsInitialized())
			EasyMobile.RuntimeManager.Init();
	}

	void makeInstance () 
	{	
		if (instance != null)
			Destroy (this.gameObject);
		else 
		{
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		}
	}

	private void OnEnable()
	{
		Advertising.RewardedAdSkipped += OnRewardedAdSkipped;
		Advertising.RewardedAdCompleted += OnRewardedAdCompleted;
		Advertising.InterstitialAdCompleted += OnInterstitialAdCompleted;
	}

	private void OnDisable()
	{
		Advertising.RewardedAdSkipped -= OnRewardedAdSkipped;
		Advertising.RewardedAdCompleted -= OnRewardedAdCompleted;
		Advertising.InterstitialAdCompleted -= OnInterstitialAdCompleted;
		//CancelInvoke ();
	}

	public void showInterstitial()
	{
			if (Advertising.IsInterstitialAdReady())
			{
				Advertising.ShowInterstitialAd();
			}
			else
			{
				if (!Advertising.IsInterstitialAdReady())
				{
					Debug.Log("[AD] AdManager::showInterstitial(), IsInterstitialAdReady = false");
					Advertising.LoadInterstitialAd();
				}
			}
	}

	void OnInterstitialAdCompleted(InterstitialAdNetwork network, AdPlacement placement)
	{
		Debug.Log("Interstitial ad has been shown+closed.");
		Analytics.CustomEvent("[AD] AdManager::showInterstitial(), Ad Completed");
    }

	public bool isRewardedAdAvailable()
    {
		bool result = Advertising.IsRewardedAdReady();

		 if (result == false)
			 Advertising.LoadRewardedAd();

		 return result;
    }

	public void showRewardedVideo () 
	{
        if (Advertising.IsRewardedAdReady())
        {
            Advertising.ShowRewardedAd();
            Debug.Log("showRewardedVideo()");
        }
        else
        {
			if (!Advertising.IsRewardedAdReady())
			{
				Debug.Log("[R-AD] AdManager::showRewardedVideo(), IsRewardedAdReady = false");
				Advertising.LoadRewardedAd();
				StubManager.instance.popup("Rewarded Ad Not Available, Try Later.");
			}
        }
    }

	void OnRewardedAdCompleted(RewardedAdNetwork network, AdPlacement placement)
	{
		Debug.Log("OnRewardedAdCompleted()");

		/*if (WelcomeBack.instance.gameObject.activeInHierarchy)
        {
			WelcomeBack.instance.rewardedVideoCompleted();
			Analytics.CustomEvent("[AD] AdManager::showRewardedVideo(), Ad Completed - Welcome Back");
		}

		else if (VehicleSelection.instance.gameObject.activeInHierarchy)
		{
			VehicleSelection.instance.rewardedVideoCompleted();
			Analytics.CustomEvent("[AD] AdManager::showRewardedVideo(), Ad Completed - Vehicle Selection");
		}

		if (GameManager.instance.gameObject.activeInHierarchy)
		{
			GameManager.instance.postAdReviveCallback();
			Analytics.CustomEvent("[AD] AdManager::showRewardedVideo(), Ad Completed - Revive");
		}*/
	}

	void OnRewardedAdSkipped(RewardedAdNetwork network, AdPlacement placement)
	{
		StubManager.instance.popup("Skipping Rewarded Ad won't give any reward");

		/*if (WelcomeBack.instance.gameObject.activeInHierarchy)
		{
			WelcomeBack.instance.rewardedVideoCompleted();
			Analytics.CustomEvent("[AD] AdManager::OnRewardedAdSkipped() - Welcome Back");
		}

		else if (VehicleSelection.instance.gameObject.activeInHierarchy)
		{
			VehicleSelection.instance.rewardedVideoCompleted();
			Analytics.CustomEvent("[AD] AdManager::OnRewardedAdSkipped() - Vehicle Selection");
		}

		if (GameManager.instance.gameObject.activeInHierarchy)
		{
			GameManager.instance.postAdReviveFailed();
			Analytics.CustomEvent("[AD] AdManager::OnRewardedAdSkipped() - Revive");
		}*/
	}

    public void showBanner()
    {
		//if (!DataManager.instance.data.iapData.adsBought)
		//{
		//	Advertising.ShowBannerAd(BannerAdPosition.Bottom);
		//}
		//else
		//{
		//	Debug.Log("ShowBannerAd() Ads were removed, wont show");
		//	Advertising.HideBannerAd();
		//}
    }

    public void hideBanner()
    {
        //Advertising.HideBannerAd();
    }
}
