﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyMobile;

public class StubTest : MonoBehaviour
{
    [SerializeField] Button unityInterstitialAdButton;
    [SerializeField] Button admobInterstitialAdButton;

    // Start is called before the first frame update
    void Start()
    {
        unityInterstitialAdButton.onClick.AddListener(() => EasyMobile.Advertising.UnityAdsClient.ShowInterstitialAd());
        admobInterstitialAdButton.onClick.AddListener(() => StubManager.instance.ads.showInterstitial());
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
