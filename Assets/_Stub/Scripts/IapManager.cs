﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Text;
using EasyMobile;
using UnityEngine.Analytics;

#if EM_UIAP
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
#endif

public class IapManager : MonoBehaviour
{
	public static IapManager instance;

	private IAPProduct selectedProduct;
	private List<IAPProduct> ownedProducts = new List<IAPProduct>();

	void OnEnable()
	{            
		InAppPurchasing.PurchaseCompleted += OnPurchaseCompleted;
		InAppPurchasing.RestoreCompleted += OnRestoreCompleted;
	}

	void OnDisable()
	{
		InAppPurchasing.PurchaseCompleted -= OnPurchaseCompleted; 
		InAppPurchasing.RestoreCompleted -= OnRestoreCompleted;
	}

	void OnPurchaseCompleted(IAPProduct product)
	{
		if (!ownedProducts.Contains(product))
			ownedProducts.Add(product);

        switch (product.Name)
        {
           /* case EM_IAPConstants.Product_Remove_Ads:
                Debug.Log("Product_Remove_Ads was purchased. The user should be granted it now.");
                DataManager.instance.data.iapData.adsBought = true;
                DataManager.instance.Save();
                break;

			case EM_IAPConstants.Product_Unlock_All___Remove_Ads:
				Debug.Log("Product_Unlock_All__Remove_Ads was purchased. The user should be granted it now.");
				VehicleSelection.instance.unlockAllCallback();
				DataManager.instance.data.iapData.adsBought = true;
				DataManager.instance.Save();
				break;*/
		}
    }

	void OnPurchaseFailed(IAPProduct product)
	{
		NativeUI.Alert("Purchased Failed", "The purchase of product " + product.Name + " has failed.");
	}

	void OnRestoreCompleted()
	{
		StartCoroutine(CROnRestoreCompleted());
	}

	IEnumerator CROnRestoreCompleted()
	{
		while (NativeUI.IsShowingAlert())
			yield return new WaitForSeconds(0.5f);

		NativeUI.Alert("Restore Completed", "Your purchases have been restored successfully.");
	}

	void Awake () 
	{
		makeInstance();
	}

	void makeInstance () 
	{	
		if(instance!=null)
			Destroy(this.gameObject);
		else
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
			InAppPurchasing.InitializePurchasing();
			print ("InAppPurchasing.IsInitialized()" + InAppPurchasing.IsInitialized ());
		}
	}

	void Start()
	{
		#if EM_UIAP
		foreach (IAPProduct p in EM_Settings.InAppPurchasing.Products)
		{
			UnityEngine.Purchasing.ProductMetadata data = InAppPurchasing.GetProductLocalizedData(p.Name);
			
			if (data != null)
			{
				Debug.Log("Product Localized Title: " + data.localizedTitle);
				Debug.Log("Localized Price: " + data.localizedPriceString);
				Debug.Log("Product Localized Description: " + data.localizedDescription);
			}
			else
			{
				Debug.Log("Localized data is null");
			}
		}
		#endif

		StartCoroutine(CheckOwnedProducts());
	}

	public void Purchase(string productName)
	{
		if (productName != null)
		{
			InAppPurchasing.Purchase(productName);

			// You can also do
			//IAPManager.Purchase(selectedProduct);

			// The advantage of method Purchase() that uses product name is you can use it with the constant product names
			// in the generated EM_IAPConstants class for compile-time error detecting.
		}
		else
		{
			NativeUI.Alert("Alert", "Please select a product.");
		}
	}

	public void RestorePurchases()
	{
		InAppPurchasing.RestorePurchases();
	}

	IEnumerator CheckOwnedProducts()
	{
		// Wait until the module is initialized
		if (!InAppPurchasing.IsInitialized())
		{
			yield return new WaitForSeconds(0.5f);
		}

		// Display list of owned non-consumable products.
		var products = EM_Settings.InAppPurchasing.Products;
		if (products != null && products.Length > 0)
		{
			for (int i = 0; i < products.Length; i++)
			{
				var pd = products[i];
				if (InAppPurchasing.IsProductOwned(pd.Name) && !ownedProducts.Contains(pd))
				{
					ownedProducts.Add(pd);
				}
			}
		}
	}

	public void removeAds()
	{
		/*Analytics.CustomEvent("[IAP] IapManager::RemoveAds() - attempt to purchase");
		if (!InAppPurchasing.IsProductOwned(EM_IAPConstants.Product_Remove_Ads))
        {
            Purchase(EM_IAPConstants.Product_Remove_Ads);
        }
        else
            Debug.Log("Product Already Owned");*/
    }

	public void unlockAll()
	{
		/*Analytics.CustomEvent("[IAP] IapManager::unlockAll() - attempt to purchase");
		if (!InAppPurchasing.IsProductOwned(EM_IAPConstants.Product_Unlock_All___Remove_Ads))
		{
			Purchase(EM_IAPConstants.Product_Unlock_All___Remove_Ads);
		}
		else
			Debug.Log("Product Already Owned");*/
	}

	public void _restore()
	{
		RestorePurchases ();
	}




}