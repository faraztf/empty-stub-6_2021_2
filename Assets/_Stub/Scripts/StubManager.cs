﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;
using EasyMobile.MiniJSON;
using EasyMobile;
using UnityEngine.Analytics;

public class StubManager : MonoBehaviour 
{
		public static StubManager instance;
		//Prefabs...
		public AdManager ads;
		public IapManager iap;

		// Notifications
		public string repeatTitle = "Hummer Offroad";
		public string repeatSubtitle = "Let's do some awesome stunts?";
		public string repeatMessage = "Are you ready for hummer stunts";
		public string repeatCategoryId;
		public int repeatDelayHours = 25;
		public int repeatDelayMinutes = 100;
		public int repeatDelaySeconds = 0;
		public NotificationRepeat repeatType = NotificationRepeat.EveryDay;

		private string androidAppUrl = "http://play.google.com/store/apps/details?id=";
		private string moreGamesUrl = "https://play.google.com/store/apps/developer?id=";
		private string facebookUrl = "https://facebook.com";
		private string youtubeUrl = "https://www.youtube.com";
		
		// Share
		public const string shareMessage = "";
		
		
		void Awake()
		{
			this.makeInstance();

			if (AdManager.instance == null)
				Instantiate(ads);

			if (IapManager.instance == null)
				Instantiate(iap);

		}

		void makeInstance () 
		{	
			if(instance!=null)
				Destroy(this.gameObject);
			else
			{
				instance = this;
				DontDestroyOnLoad(this.gameObject);
			}
		}

		void Start () 
		{
			androidAppUrl+=Application.identifier;
			Notifications.Init();
		}

		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				if (Application.platform == RuntimePlatform.Android)
				{
					/*if (Quit.instance)
						Quit.instance.show();*/
				}
			}
		}


    public void manualRate()
		{
			/*Analytics.CustomEvent("StubManager::manualRate() - Opted to rate at Level="+(DataManager.instance.data.currentLevel+1).ToString());
			DataManager.instance.data.isRated = true;
			DataManager.instance.Save();
			
			SoundManager.instance.tap();
			#if UNITY_ANDROID
			Application.OpenURL(androidAppUrl);
			#elif UNITY_IOS
			// iOS8/+ Only
			Application.OpenURL(iOSAppURL);
			#else
			#endif*/
		}

		public void moreGames()
		{
			/*Analytics.CustomEvent("StubManager::moreGames()");
			SoundManager.instance.tap();
			#if UNITY_ANDROID
			Application.OpenURL(moreGamesUrl);
			#endif*/
		}
		
		public void facebook()
		{
			/*Analytics.CustomEvent("StubManager::facebook()");
			SoundManager.instance.tap();
			#if UNITY_ANDROID
			Application.OpenURL(facebookUrl);
			#endif*/
		}

		public void youtube()
		{
			/*Analytics.CustomEvent("StubManager::youtube()");
			SoundManager.instance.tap();
			#if UNITY_ANDROID
			Application.OpenURL(youtubeUrl);
			#endif*/
		}
		
		public void autoRate()
		{
			//Rate.instance.show ();
		}

		public void localNotification()
		{
			/*var notif = new NotificationContent();
			notif.title = repeatTitle;
			notif.body = repeatMessage;
			notif.categoryId = repeatCategoryId;

			Notifications.ScheduleLocalNotification(new TimeSpan(repeatDelayHours, repeatDelayMinutes, repeatDelaySeconds), notif, repeatType);*/
		}

		public void share()
		{
			/*Analytics.CustomEvent("StubManager::share()");
			SoundManager.instance.tap();
			Sharing.ShareText(androidAppUrl);*/
		}


		void OnApplicationPause( bool pauseStatus )
		{
			/*if (pauseStatus) 
			{
			localNotification ();
			}
			else
			{
			Notifications.ClearAllDeliveredNotifications ();
			Notifications.CancelAllPendingLocalNotifications();
			}*/
		}
		
		public void popup(string message)
		{
			Debug.Log(message);
			#if(UNITY_EDITOR)
			Debug.Log(message);
			#else
			NativeUI.Alert("Alert",message);
			#endif
		}

}